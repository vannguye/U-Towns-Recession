# University Towns less affected during Recessions

This project was tasked to determine if university town housing prices are less affected by recessions than non university town housing prices. I use a t-test comparing the ratio of the housing prices before the recession vs. the recession bottom.   