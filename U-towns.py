import pandas as pd

import numpy as np

from scipy.stats import ttest_ind

states = {'OH': 'Ohio', 'KY': 'Kentucky', 'AS': 'American Samoa', 'NV': 'Nevada', 'WY': 'Wyoming', 'NA': 'National', 'AL': 'Alabama', 'MD': 'Maryland', 'AK': 'Alaska', 'UT': 'Utah', 'OR': 'Oregon', 'MT': 'Montana', 'IL': 'Illinois', 'TN': 'Tennessee', 'DC': 'District of Columbia', 'VT': 'Vermont', 'ID': 'Idaho', 'AR': 'Arkansas', 'ME': 'Maine', 'WA': 'Washington', 'HI': 'Hawaii', 'WI': 'Wisconsin', 'MI': 'Michigan', 'IN': 'Indiana', 'NJ': 'New Jersey', 'AZ': 'Arizona', 'GU': 'Guam', 'MS': 'Mississippi', 'PR': 'Puerto Rico', 'NC': 'North Carolina', 'TX': 'Texas', 'SD': 'South Dakota', 'MP': 'Northern Mariana Islands', 'IA': 'Iowa', 'MO': 'Missouri', 'CT': 'Connecticut', 'WV': 'West Virginia', 'SC': 'South Carolina', 'LA': 'Louisiana', 'KS': 'Kansas', 'NY': 'New York', 'NE': 'Nebraska', 'OK': 'Oklahoma', 'FL': 'Florida', 'CA': 'California', 'CO': 'Colorado', 'PA': 'Pennsylvania', 'DE': 'Delaware', 'NM': 'New Mexico', 'RI': 'Rhode Island', 'MN': 'Minnesota', 'VI': 'Virgin Islands', 'NH': 'New Hampshire', 'MA': 'Massachusetts', 'GA': 'Georgia', 'ND': 'North Dakota', 'VA': 'Virginia'}

def get_list_of_university_towns():
    
    file = open(r'C:\Users\10495424\Desktop\U-towns Recession Project\university_towns.txt')
    u_towns = file.readlines()
    
    list_u_towns = []
    for RegionName in u_towns:
        if '[edit]' in RegionName:
            State = RegionName.split('[')[0]
        elif ' (' in RegionName:
            RegionName = RegionName.split(' (')[0]
            list_u_towns.append([State, RegionName])
    
    df = pd.DataFrame(list_u_towns, columns = ['State', 'RegionName'])
    
    return df
    
#print(get_list_of_university_towns())

def get_recession_start():
    GDP = pd.read_excel(r'C:\Users\10495424\Desktop\U-towns Recession Project\gdplev.xls').filter(['Unnamed: 4', 'Unnamed: 6'])
    GDP = GDP[219:]
    GDP.columns = ['Quarter', 'GDP']
    
    last_GDP = 0
    conseq = 0
    
    for index, row in GDP.iterrows():
        curr_GDP = row['GDP']
        
        if(curr_GDP < last_GDP):
            conseq += 1
            if(conseq == 1):
                recession_start = row['Quarter']
        else:
            conseq = 0
                
        if(conseq == 2):
            break
        
        last_GDP = curr_GDP
    return recession_start

print(get_recession_start())

def get_recession_end():
    GDP = pd.read_excel(r'C:\Users\10495424\Desktop\U-towns Recession Project\gdplev.xls').filter(['Unnamed: 4', 'Unnamed: 6'])
    GDP = GDP[219:]
    GDP.columns = ['Quarter', 'GDP']
    
    last_GDP = 0
    conseq = 0
    during_recession = False
    
    for index, row in GDP.iterrows():
        curr_GDP = row['GDP']
        
        if(during_recession == False):
            if(curr_GDP < last_GDP):
                conseq += 1
            else:
                conseq = 0
            if(conseq == 2):
                during_recession = True
                conseq = 0
                
            last_GDP = curr_GDP
        
        else:
            if(curr_GDP > last_GDP):
                conseq += 1
            else:
                conseq = 0
                
            if(conseq == 2):
                recession_end = row['Quarter']
                break
            
            last_GDP = curr_GDP
            
    return recession_end

print(get_recession_end())

def get_recession_bottom():
    GDP = pd.read_excel(r'C:\Users\10495424\Desktop\U-towns Recession Project\gdplev.xls').filter(['Unnamed: 4', 'Unnamed: 6'])
    GDP = GDP[219:]
    GDP.columns = ['Quarter', 'GDP']
    
    last_GDP = 0
    conseq = 0
    during_recession = False
    least_GDP = float('inf')
    
    for index, row in GDP.iterrows():
        curr_GDP = row['GDP']
        
        if(during_recession == False):
            if(curr_GDP < last_GDP):
                conseq += 1
            else:
                conseq = 0
            if(conseq == 2):
                during_recession = True
                conseq = 0
                
            last_GDP = curr_GDP
        
        else:
            if(curr_GDP < least_GDP):
                least_GDP = curr_GDP
                recession_bottom = row['Quarter']
                
            if(curr_GDP > last_GDP):
                conseq += 1
            else: 
                conseq = 0
                
            if(conseq == 2):
                break
            
            last_GDP = curr_GDP
        
    return recession_bottom

print(get_recession_bottom())

def convert_housing_data_to_quarters():
    housing = pd.read_csv(r'C:\Users\10495424\Desktop\U-towns Recession Project\City_Zhvi_AllHomes.csv')
    
    year = 2000
    q = 1
    for i in range(51,251,3):
        housing[str(year) + 'q' + str(q)] = housing.iloc[:, i:i+3].mean(axis=1)
        if(q == 4):
            q = 1
            year += 1
        else:
            q += 1
            
    housing = housing.iloc[:, np.r_[1,2, 251:318]]
    
    states = {'OH': 'Ohio', 'KY': 'Kentucky', 'AS': 'American Samoa', 'NV': 'Nevada', 'WY': 'Wyoming', 'NA': 'National', 'AL': 'Alabama', 'MD': 'Maryland', 'AK': 'Alaska', 'UT': 'Utah', 'OR': 'Oregon', 'MT': 'Montana', 'IL': 'Illinois', 'TN': 'Tennessee', 'DC': 'District of Columbia', 'VT': 'Vermont', 'ID': 'Idaho', 'AR': 'Arkansas', 'ME': 'Maine', 'WA': 'Washington', 'HI': 'Hawaii', 'WI': 'Wisconsin', 'MI': 'Michigan', 'IN': 'Indiana', 'NJ': 'New Jersey', 'AZ': 'Arizona', 'GU': 'Guam', 'MS': 'Mississippi', 'PR': 'Puerto Rico', 'NC': 'North Carolina', 'TX': 'Texas', 'SD': 'South Dakota', 'MP': 'Northern Mariana Islands', 'IA': 'Iowa', 'MO': 'Missouri', 'CT': 'Connecticut', 'WV': 'West Virginia', 'SC': 'South Carolina', 'LA': 'Louisiana', 'KS': 'Kansas', 'NY': 'New York', 'NE': 'Nebraska', 'OK': 'Oklahoma', 'FL': 'Florida', 'CA': 'California', 'CO': 'Colorado', 'PA': 'Pennsylvania', 'DE': 'Delaware', 'NM': 'New Mexico', 'RI': 'Rhode Island', 'MN': 'Minnesota', 'VI': 'Virgin Islands', 'NH': 'New Hampshire', 'MA': 'Massachusetts', 'GA': 'Georgia', 'ND': 'North Dakota', 'VA': 'Virginia'}
    
    housing.replace({'State': states}, inplace=True)
    
    housing.set_index(['State', 'RegionName'], inplace=True)
    
    return housing

print(convert_housing_data_to_quarters())
    
def run_ttest():
    u_towns = get_list_of_university_towns()
    housing = convert_housing_data_to_quarters()
    start_qtr = get_recession_start()
    end_qtr = get_recession_end()
    bot_qtr = get_recession_bottom()
    
    start_i = housing.columns.get_loc(start_qtr)
    end_i = housing.columns.get_loc(end_qtr)

    housing = housing.iloc[:, start_i:end_i].reset_index()
    housing['Price Ratio'] = housing[start_qtr]/housing[bot_qtr]
    
    
    u_towns_data = pd.merge(u_towns, housing, left_on=['State','RegionName'], right_on=['State','RegionName'], how='inner', indicator=True)
    non_u_towns_data = pd.merge(u_towns, housing, left_on=['State','RegionName'], right_on=['State','RegionName'], how='outer', indicator=True)
    non_u_towns_data = non_u_towns_data[non_u_towns_data['_merge'] == 'right_only']
    
    ttest_res = ttest_ind(u_towns_data['Price Ratio'], non_u_towns_data['Price Ratio'], nan_policy='omit')
    
    reject_null = None
    lower_mean_price_ratio = None
    
    if ttest_res[1] < 0.01:
        reject_null = True
    else:
        reject_null = False
    if ttest_res[0] < 0:
        lower_mean_price_ratio = 'university town'
    else:
        lower_mean_price_ratio = 'non-university_town'
    
    
    return reject_null, ttest_res[1], lower_mean_price_ratio
    
    
    
    
    
    
    
    
    
    
    

